package userinterface;

import java.util.Scanner;

import app.*;
//Theodor
public class ConsoleInterface {

	static Application app;
	static Scanner s;

	public static void main(String[] args) throws OperationNotAllowedException {
		app = new Application();
		s = new Scanner(System.in);
		ConsoleMainMenu main = new ConsoleMainMenu(s, app);
		commandLoop(main);
	}

	public static void commandLoop(Menu menu) throws OperationNotAllowedException {
		while (true) {
			menu.printMenu();

			String line = s.nextLine();

			if(line.isEmpty()) {
				System.out.println("Kan ikke tage imod et tomt input \n");
			}

			if (line.equals("q")) {
				System.out.println("Exit successful");
				return;
			}
			
			menu.checkCommandLine(line);
		}
	}
}
