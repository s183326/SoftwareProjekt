package userinterface;

import java.util.Scanner;
import app.*;
//Theodor
public class ConsoleMainMenu implements Menu {

	Scanner s;
	Application app;
	ConsoleViewMenu view;

	public ConsoleMainMenu(Scanner s, Application app) {
		this.s = s;
		this.app = app;
		view = new ConsoleViewMenu(s, app);

	}

	public void printMenu() {
		System.out.println("|-----------------------------------|");
		System.out.println("  1. Add employee");
		System.out.println("  2. Assign employee to project manager");
		System.out.println("  3. Add project");
		System.out.println("  4. Add project manager to project");
		System.out.println("  5. Add activity to project");
		System.out.println("  6. Set deadline for project");
		System.out.println("  7. Assign employee to project");
		System.out.println("  8. Assign employee to activity");
		System.out.println("  9. Employee requests assistance");
		System.out.println("  10. Employee register time spend on activity");
		System.out.println("  11. Remove employee");
		System.out.println("  12. Project manager removes project");
		System.out.println("  13. View menu");
		System.out.println("|-----------------------------------|");
		System.out.print("Enter number: ");
	}

	public void checkCommandLine(String line) {
		switch (line) {
		case "1":
			addEmployee(false);
			break;
		case "2":
			assignEmployeeToProjectManager();
			break;
		case "3":
			addProject();
			break;
		case "4":
			addProjectmanagerToProject();
			break;
		case "5":
			addActivityToProject();
			break;
		case "6":
			setDeadlineForProject();
			break;
		case "7":
			assignEmployeeToProject();
			break;
		case "8":
			assignEmployeeToActivity();
			break;
		case "9":
			employeeRequestAssistance();
			break;
		case "10":
			employeeRegistersTime();
			break;
		case "11":
			removeEmployee();
			break;
		case "12":
			projectManagerRemovesProject();
			break;
		case "13":
			try {
				ConsoleInterface.commandLoop(view);
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
			}
			break;
		}
	}
//Victor
	private void removeEmployee() {
		System.out.print("Enter employee initials: ");
		String initials = s.next();

		if (quitOperation(initials)) {
			s.nextLine();
			return;
		}

		try {
			app.removeEmployee(initials);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}
		System.out.println("Employee with initials: " + initials + " was removed from system successfully.");
		s.nextLine();
	}

	private void projectManagerRemovesProject() {
		System.out.print("Enter project id and project manager initials: ");
		String line = s.next();

		if (quitOperation(line)) {
			s.nextLine();
			return;
		}

		int projectID = 0;
		try {
			projectID = checkInteger(line);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		String initials = s.next();

		try {
			app.projectManagerRemoveProject(projectID, initials);
			;
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}
		System.out.println("Project with id: " + projectID + " was removed from the system successfully.");
		s.nextLine();
	}
//Bastian
	private void employeeRegistersTime() {
		System.out.print("Enter employee initials, project id, activity name and hours spend: ");
		String initials = s.next();
		if (quitOperation(initials)) {
			s.nextLine();
			return;
		}
		String line = s.next();
		String activityName = s.next();
		String line2 = s.next();

		int projectID = 0;
		int timeSpend = 0;

		try {
			projectID = checkInteger(line);
			timeSpend = checkInteger(line2);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		try {
			app.registerTimeSpendOnActivity(initials, projectID, activityName, timeSpend);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		s.nextLine();
	}

	private void assignEmployeeToProjectManager() {
		System.out.print("Enter employee initials: ");
		String initials = s.next();

		if (quitOperation(initials)) {
			s.nextLine();
			return;
		}

		try {
			app.assignEmployeeToProjectManager(initials);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}
		System.out.println("Assigned " + initials + " to project manager succesfully");
		s.nextLine();
	}

	private void employeeRequestAssistance() {
		System.out.print("Enter your initials, initials of helper and name of activity");
		String initial1 = s.next();

		if (quitOperation(initial1)) {
			s.nextLine();
			return;
		} else {
			String initial2 = s.next();
			String activityName = s.next();
			try {
				app.employeeAskForAssistance(initial1, initial2, activityName);
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
				s.nextLine();
				return;

			}
		}
		s.nextLine();
	}
//Cecilie
	private void assignEmployeeToActivity() {
		System.out.print("press 1 for private activity or 2 for project activity: ");
		String line = s.next();

		if (quitOperation(line)) {
			s.nextLine();
			return;
		}

		if (line.equals("1")) {
			System.out.print("Enter name of activity, initials and duration of the activity: ");

			String activityName = s.next();

			if (quitOperation(activityName)) {
				s.nextLine();
				return;
			}

			String initialsE = s.next();
			// String line2 = s.next();
			int duration = 0;
			try {
				duration = checkInteger(s.next());
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
				s.nextLine();
				return;
			}

			try {
				app.assignPrivateActivity(initialsE, activityName, duration);
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
				s.nextLine();
				return;
			}
			System.out.println("Added employee to activity successfully");
			s.nextLine();

		} else if (line.equals("2")) {
			System.out.print("Enter project id, activity name, initials of employe and project manager: ");
			String line1 = s.next();

			if (quitOperation(line1)) {
				s.nextLine();
				return;
			}

			int projectID = 0;

			try {
				projectID = checkInteger(line1);
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
				s.nextLine();
				return;
			}

			String activityName = s.next();
			String initialsE = s.next();
			String initialsP = s.next();
			try {
				app.assignEmployeeToActivity(projectID, activityName, initialsE, initialsP);
			} catch (OperationNotAllowedException e) {
				System.out.println(e.getMessage() + "\n");
				// e.printStackTrace();
				s.nextLine();
				return;
			}
			System.out.println("Added employee to activity successfully");
			s.nextLine();
		}
	}

	private void assignEmployeeToProject() {
		System.out.print("Enter project id, employee initials and projectmanager initials: ");

		String line = s.next();

		if (quitOperation(line)) {
			s.nextLine();
			return;
		}
		int ID = 0;

		try {
			ID = checkInteger(line);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		String initials = s.next();
		String managerInitials = s.next();
		try {
			app.addEmployeeToProject(ID, initials, managerInitials);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			// e.printStackTrace();
			s.nextLine();
			return;
		}
		System.out.println("Added employee to project successfully");
		s.nextLine();
	}
//Alexander
	private void setDeadlineForProject() {
		System.out.print("Enter project manager initials, project id, new end week and end year: ");
		String initialP = s.next();

		if (quitOperation(initialP)) {
			s.nextLine();
			return;
		}

		String line = s.next();
		String line2 = s.next();
		String line3 = s.next();

		int projectID;
		int endWeek;
		int endYear;

		try {
			projectID = checkInteger(line);
			endWeek = checkInteger(line2);
			endYear = checkInteger(line3);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		try {
			app.projectManagerSetsDeadlineForProject(initialP, projectID, endWeek, endYear);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			// e.printStackTrace();
			s.nextLine();
			return;
		}
		System.out.println("Added deadline successfully");
		s.nextLine();
	}

	private void addProjectmanagerToProject() {
		System.out.print("Enter project id and initials: ");

		String line = s.next();

		if (quitOperation(line)) {
			s.nextLine();
			return;
		}
		int id;
		try {
			id = checkInteger(line);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		String initials = s.next();
		try {
			app.addProjectmanagerToProject(initials, id);
		} catch (OperationNotAllowedException e) {
			System.out.println("errormessage was " + e.getMessage());
			// e.printStackTrace();
			s.nextLine();
			return;
		}
		System.out.println("Added project manager to project successfully");
		s.nextLine();
	}

	private void addActivityToProject() {
		System.out.print("Enter name of activity, expected time, project id and initials: ");
		String name = s.next();

		if (quitOperation(name)) {
			s.nextLine();
			return;
		}

		String line = s.next();
		String line2 = s.next();

		int expectedTime;
		int projectID;

		try {
			expectedTime = checkInteger(line);
			projectID = checkInteger(line2);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		String initials = s.next();

		try {
			app.assignActivityToProject(name, expectedTime, projectID, initials);
		} catch (OperationNotAllowedException e) {
			System.out.println("errormessage was " + e.getMessage());
			s.nextLine();
			return;
		}
		System.out.println("Added activity successfully");
		s.nextLine();
	}

	public void addProject() {
		System.out.print("Enter year, start week, end week: ");
		String line = s.next();

		if (quitOperation(line)) {
			s.nextLine();
			return;
		}
		String line2 = s.next();
		String line3 = s.next();

		int year;
		int start;
		int end;
		
		try {
			year = checkInteger(line);
			start = checkInteger(line2);
			end = checkInteger(line2);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}

		try {
			app.addProject(year, start, end);

		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			s.nextLine();
			return;
		}
		s.nextLine();
		System.out.println("The project was added successfully");
	}

//Theodor
	private void addEmployee(boolean pm) {
		System.out.print("Enter initials: ");
		String initials = s.nextLine();

		if (quitOperation(initials)) {
			return;
		}

		try {
			app.addEmployee(initials, pm);
		} catch (OperationNotAllowedException e) {
			System.out.println(e.getMessage() + "\n");
			return;
		}
		System.out.println("Added an employee " + initials + " successfully.\n");
	}

	private boolean quitOperation(String toQuit) {
		if (toQuit.equals("q")) {
			System.out.println("Exit successful");
			return true;
		}
		return false;
	}
//Alexander
	private Integer checkInteger(String toCheck) throws OperationNotAllowedException {
		int toReturn;
		try {
			toReturn = Integer.parseInt(toCheck);
			return toReturn;
		} catch (NumberFormatException xD) {
			throw new OperationNotAllowedException("Some input must be of type Integer");
		}

	}
}
