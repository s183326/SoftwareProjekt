package userinterface;

import java.util.ArrayList;
import java.util.Scanner;
import app.*;
//Bastian
public class ConsoleViewMenu implements Menu {
	
	Application app;
	Scanner s;
	ConsoleProjectMenu projectMenu;
	
	public ConsoleViewMenu(Scanner s, Application app) {
		this.app = app;
		this.s = s;
	}
	
	public void printMenu() {
		// Print hvor manger projekter der er
		// Hvor mange timer der brugt for alle projekter osv alt muligt data.
		System.out.println("Project counter: "+app.projectCounter);
		System.out.println("|-----------------------------------|");
		ArrayList<Project> projects = app.getProjects();
		for (int i = 0; i < projects.size(); i++) {
			System.out.println(projects.get(i).getID());
		}
		System.out.println("|-----------------------------------|");
		System.out.print("Enter project ID: ");
	}
//Theodor
	public void checkCommandLine(String line) {
		int id=0;
		try {
		id = Integer.parseInt(line);

		}catch(NumberFormatException e) {
			System.out.println("Project number is of type Integer");
			return;
		}
		
		Project project = null;
		try {
			project = app.getProject(id);
		} catch (OperationNotAllowedException e1) {
			System.out.println("errormessage was " + e1.getMessage());
			return;
		}
		projectMenu = new ConsoleProjectMenu(project, s);
		
		try {
			ConsoleInterface.commandLoop(projectMenu);
		} catch (OperationNotAllowedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
