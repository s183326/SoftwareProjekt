package userinterface;

import java.util.ArrayList;
import java.util.Scanner;
import app.*;
//Victor
public class ConsoleProjectMenu implements Menu {

	Project project;
	Scanner s;
	
	public ConsoleProjectMenu(Project project, Scanner s) {
		this.project = project;
		this.s = s;
	}
	
	@Override
	public void printMenu() {
		System.out.println();
		// project data her
		System.out.println("Number of employees: " +project.getEmployees().size());
		System.out.println("End year of project: " +project.getYear());
		System.out.println("Start week of project: " +project.getStartWeek());
		System.out.println("Deadline week for project: " +project.getDeadline());
		
		ArrayList<Activity> activities = project.getActivities();
		int totalTime = 0;
		for (Activity a : activities) {
			ArrayList<Integer> timeS = a.getTimeSpend();
			for (int i = 0; i < timeS.size(); i++) {
				totalTime += timeS.get(i);
			}
		}
		System.out.println("Total hours spend on project: " + totalTime);
		
		
		System.out.println("|-----------------------------------|");
		for (int i = 0; i < activities.size(); i++) {
			System.out.println(activities.get(i).getName());
		}
		System.out.println("|-----------------------------------|");
		System.out.print("Enter activity: ");
	}
//Cecilie
	@Override
	public void checkCommandLine(String line) {
		System.out.println();
		String activityName = line;
		
		Activity a = null;
		try {
			a = project.searchActivity(activityName);
		} catch (OperationNotAllowedException e1) {
			System.out.println(e1.getMessage() + "\n");
			return;
		}
		
		System.out.println("|-----------------------------------|");
		System.out.println("Activity name: " + a.getName());
		System.out.println("Expected weeks for activity completion: " + a.getExpectedTime() +"\n");
		for (Employee e : a.getEmployees()) {
			System.out.println("Employee: " + e.initials + " has spend " + a.timeSpendForEmployee(e) + " hours on the project");
		}
		System.out.println("|-----------------------------------|");
		System.out.println("Press enter to return");
		s.nextLine();
	}

}
