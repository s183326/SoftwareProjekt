package app;

import java.util.ArrayList;
import app.OperationNotAllowedException;
//Theodor
public class Application {
	ArrayList<Employee> employees;
	ArrayList<Project> projects;
	ArrayList<Activity> privateActivities;
	public int projectCounter;

	public Application() {
		employees = new ArrayList<Employee>();
		projects = new ArrayList<Project>();
		projectCounter = 0;
	}

	public void addEmployee(String initials, boolean pm) throws OperationNotAllowedException {
		assert initials != null: "Preconditions violated for addEmployee";
		
		if (initials.length() > 4 || initials.length() < 1) {                      // 1
			throw new OperationNotAllowedException("Invalid employee initials");
		}
		if (checkEmployee(initials)) {                                             // 2
			throw new OperationNotAllowedException("Employee is already registered");
		}
		Employee e;
		if (!pm) {																   // 3
			e = new Employee(initials);
		} else {                                                                   // 4
			e = new ProjectManager(initials);
		}
		employees.add(e);
		
		assert checkEmployee(initials): "Postcondition violated for addEmployee";
	}
	
	public void removeEmployee(String initials) throws OperationNotAllowedException {
		for(Activity activity : getEmployee(initials).activities) {
			activity.removeEmployee(initials);
			activity.getProject().removeEmployee(initials);
		}
		employees.remove(getEmployee(initials));
	}
	
	public void assignEmployeeToProjectManager(String initials) throws OperationNotAllowedException {
			Employee e = getEmployee(initials);
			employees.remove(e);
			ProjectManager pm = new ProjectManager(initials);
			employees.add(pm);
	}

	public boolean checkEmployee(String initials) {
		for (Employee emp : employees) {
			if (emp.initials.equals(initials)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean checkProject(int ID) {
		for (Project project: projects) {
			if (project.getID() == ID) {
				return true;
			}
		}
		return false;
	}

	public void addProject(int year, int startWeek, int endWeek) throws OperationNotAllowedException {
		Project project = new Project(year, startWeek, endWeek);
		
		if (year <= 0 || startWeek <= 0 || endWeek <= 0) {						//1
			throw new OperationNotAllowedException("Given invalid parameters");
		} else {																//2
			projectCounter++;
			project.setID(projectCounter);
			projects.add(project);
		}
		
		assert projects.contains(project): "Postcondition violated for addProject";
	}
	
	public void projectManagerRemoveProject(int ID, String initials) throws OperationNotAllowedException {
		if(initials.equals(getProject(ID).getProjectManager().getInitials())) {
			projects.remove(getProject(ID));
			projectCounter--;
		}else {
			throw new OperationNotAllowedException("Only the project manager can remove the project");
		}
	}

	public Project getProject(int id) throws OperationNotAllowedException {
		for (Project p : projects) {
			if (p.getID() == id) {
				return p;
			}
		}
		throw new OperationNotAllowedException("Project does not exist in the system");
	}
	//Alexander
	public Employee getEmployee(String initials) throws OperationNotAllowedException {
		for (Employee e : employees) {
			if (e.getInitials().equals(initials)) {
				return e;
			}
		}
		throw new OperationNotAllowedException("Employee does not exist in the system");
	}

	public void addProjectmanagerToProject(String managerInitials, int id) throws OperationNotAllowedException {
		Project project = getProject(id);
		if(!(getEmployee(managerInitials) instanceof ProjectManager)) {
			throw new OperationNotAllowedException("Employee cannot be a project manager");
		}
		ProjectManager projectmanager = (ProjectManager) getEmployee(managerInitials);
		project.assignProjectmanager(projectmanager);
		projectmanager.managerOfProjects.add(project);
	}

	public void assignActivityToProject(String name, int expectedTime, int ID, String initials)
			throws OperationNotAllowedException {
		getProject(ID).assignActivity(name, expectedTime, getEmployee(initials));
		
	}

	public void addEmployeeToProject(int ID, String initials, String managerInitials) throws OperationNotAllowedException {
		getProject(ID).assignEmployee(getEmployee(initials), getEmployee(managerInitials));
	}
	
	public void assignPrivateActivity(String initials, String name, int duration) throws OperationNotAllowedException {
		getEmployee(initials).addPrivateActivity(name, duration);
	}

	public ArrayList<Project> getProjects() {
		return projects;
	}

	public void assignEmployeeToActivity(int projectID, String activityName, String initialsE, String initialsP) throws OperationNotAllowedException {
		if(!(getEmployee(initialsP).getClass() == ProjectManager.class)) {
			throw new OperationNotAllowedException("employee is not project manager");
		}
		
		Project p = getProject(projectID);
		Activity a = p.searchActivity(activityName);
		
		a.assignEmployee(getEmployee(initialsE), (ProjectManager)getEmployee(initialsP));
	}
	
	public void employeeAskForAssistance(String initial1, String initial2, String activityName) throws OperationNotAllowedException {
		Employee e1 = getEmployee(initial1);
		Employee e2 = getEmployee(initial2);
		for (Activity a : e1.activities) {
			if (a.getName().equals(activityName)) {
				e1.askForAssistance(e2, a);
				return;
			}
		}
		throw new OperationNotAllowedException("Employee did not have an activity with that name");
	}
	//Victor
	public void projectManagerSetsDeadlineForProject(String initialP, int projectID, int endWeek, int endYear) throws OperationNotAllowedException {
		if(!(getEmployee(initialP).getClass() == ProjectManager.class)) {
			throw new OperationNotAllowedException("Employee cannot set a deadline for a project");
		}
		ProjectManager pm = (ProjectManager) getEmployee(initialP);
		pm.setDeadline(getProject(projectID), endWeek, endYear);
	}

	public void registerTimeSpendOnActivity(String initials, int projectID, String activityName, int timeSpend) throws OperationNotAllowedException {
		Employee e = getEmployee(initials);
		Project p = getProject(projectID);
		Activity a = p.searchActivity(activityName);
		a.registerTimeSpend(e, timeSpend);
	}

}
