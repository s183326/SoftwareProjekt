package app;

import java.util.ArrayList;
//Victor
public class Employee {
	public String initials;
	public ArrayList<Activity> activities = new ArrayList<Activity>();
	
	public Employee(String initials) {
		this.initials = initials;
	}
	
	public void addPrivateActivity(String name, int duration) {
		Activity activity = new Activity(name, duration, this);
		activities.add(activity);
	}
	
	public boolean hasActivity(Activity act) {
		for(Activity activity : activities) {
			if(activity.getName().equals(act.getName()) && activity.getExpectedTime() == act.getExpectedTime()) {
				return true;
			}
		}
		return false;
	}
	
	public String getInitials() {
		return initials;
	}
	
	public void askForAssistance(Employee e, Activity a) {
		a.assignEmployeeWithoutManager(e);
	}
	
	public boolean isProjectManager(Project p) throws OperationNotAllowedException {
		if(getInitials().equals(p.getProjectManager().getInitials())) {
			return true;
		}
		return false;
	}
}