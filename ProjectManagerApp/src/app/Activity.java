package app;

import java.util.ArrayList;
//Bastian
public class Activity {
	
	private String name;
	private int expectedTime;
	private ArrayList<Employee> employees = new ArrayList<Employee>();
	private Project project;
	
	// Time collector
	private ArrayList<Integer> timeSpend = new ArrayList<Integer>();
	
	
	public Activity(String name, int expectedTime) {
		this.name = name;
		this.expectedTime = expectedTime;
	}
	
	//Constructor for private activities
	public Activity(String name, int duration, Employee employee) {
		this.name = name;
		this.expectedTime = duration;
		employees.add(employee);
	}
	
	public String getName() {
		return name;
	}
	
	public Project getProject() {
		return project;
	}
	
	public int getExpectedTime() {
		return expectedTime;
	}
	
	public ProjectManager getProjectManager() throws OperationNotAllowedException {
		return project.getProjectManager();
	}
	
	public void assignProject(Project project) {
		this.project = project;
	}
	
	public void assignEmployee(Employee e, ProjectManager projectManager) throws OperationNotAllowedException {
		assert e != null && projectManager != null : "Precondition violated for assignEmployee";
		
		if(projectManager.getInitials() != project.getProjectManager().getInitials()) { 		// 1
			throw new OperationNotAllowedException("Invalid project manager");
		} else if(e.activities.size() >= 20) {													// 2
			throw new OperationNotAllowedException("Employee has too many activities");
		} else {																				// 3
			employees.add(e);
			e.activities.add(this);
			timeSpend.add(0);
		}
		assert employees.contains(e) && e.activities.contains(this) : "Postcondition violated for assignEmployee";
	}
	
	public boolean containsEmployee(Employee e) {
		for(Employee employee : employees) {
			if(employee.getInitials().equals(e.getInitials())) {
				return true;
			}
		}
		return false;
	}
	//Cecilie
	public void assignEmployeeWithoutManager(Employee e) {
		employees.add(e);
		e.activities.add(this);
		timeSpend.add(0);
	}

	public void registerTimeSpend(Employee e, int time) throws OperationNotAllowedException {
		if (time <= 0) {
			throw new OperationNotAllowedException("Can not register negativ time");
		}
		int index = employees.indexOf(e);
		
		int t = timeSpend.get(index);
		t += time;
		timeSpend.set(index, t);
	}
	
	public int timeSpendForEmployee(Employee e) {
		int index = employees.indexOf(e);
		return timeSpend.get(index);
	}
	
	public ArrayList<Integer> getTimeSpend() {
		return timeSpend;
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	public void removeEmployee(String id) {
		for(Employee employee : employees) {
			if(employee.getInitials().equals(id)) {
				employees.remove(employee);
				return;
			}
		}
	}

}
