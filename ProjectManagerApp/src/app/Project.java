package app;

import java.util.ArrayList;
//Victor
public class Project {

	private int ID;
	private int startWeek;
	private int endWeek;
	private int year;

	private ProjectManager projectmanager;
	private ArrayList<Activity> activities;
	private ArrayList<Employee> employees;

	public Project(int year, int startWeek, int endWeek) {
		this.year = year;
		this.startWeek = startWeek;
		this.endWeek = endWeek;
		activities = new ArrayList<Activity>();
		employees = new ArrayList<Employee>();
	}

	public int getID() { 
		return ID;
	}

	public int getYear() {
		return year;
	}
	public int getStartWeek() {
		return startWeek;
	}

	public void setID(int projectCounter) {
		ID = projectCounter;
	}

	public void assignProjectmanager(ProjectManager projectmanager) throws OperationNotAllowedException {
		if (this.projectmanager == projectmanager) {
			throw new OperationNotAllowedException("Cannot add the same projectmanager");
		}
		this.projectmanager = projectmanager;
	}

	public void assignEmployee(Employee employee, Employee projectManager) throws OperationNotAllowedException {
		if(!projectManager.getInitials().equals(this.projectmanager.getInitials())) {
			throw new OperationNotAllowedException("Only the project manager can assign an employee to a project");
		} else {
			employees.add(employee);
		}
	}

	public int getDeadline() {
		return endWeek;
	}
	//Cecilie
	public ProjectManager getProjectManager() throws OperationNotAllowedException {
		if(projectmanager == null) {
			throw new OperationNotAllowedException("There is no projectmanager assigned to the project");
		}
		return projectmanager;
	}

	public void assignActivity(String name, int expectedTime, Employee employee) throws OperationNotAllowedException {
		if(!employee.isProjectManager(this)) {
			throw new OperationNotAllowedException("Only the project manager can assign an activity");
		}
		Activity activity = new Activity(name, expectedTime);
		activities.add(activity);
		activity.assignProject(this);
	}

	public boolean hasActivity(Activity act) {
		for(Activity activity : activities) {
			if(activity.getName() == act.getName() && activity.getExpectedTime() == act.getExpectedTime()) {
				return true;
			}
		}
		return false;
	}

	public Activity searchActivity(String name) throws OperationNotAllowedException {
		assert name != null: "Preconditions violated setDeadline";

		for(Activity activity : activities) {				// 1
			if(activity.getName().equals(name)) {			// 2
				return activity;
			}
		}
		throw new OperationNotAllowedException("Activity does not exist in project");
	}
	//Bastian
	public void setDeadline(int endWeek, int endYear, Employee emp) throws OperationNotAllowedException {

		assert emp != null: "Preconditions violated setDeadline";
		if(!(emp.getClass() == ProjectManager.class)) {															  //1
			throw new OperationNotAllowedException("Only project managers are allowed to change the deadline");

		} else if(endWeek < startWeek && endYear <= year) {														  //2
			throw new OperationNotAllowedException("Invalid deadline");

		} else {																								  //3
			this.endWeek = endWeek;
			year = endYear;
		}
		assert (this.endWeek == endWeek && year == endYear): "Postconditions violated setDeadline";
	}
	
	public boolean checkEmployee(String initials) {
		for (Employee emp : employees) {
			if (emp.initials.equals(initials)) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Activity> getActivities() {
		return activities;
	}

	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	public void removeEmployee(String id) {
		for(Employee employee : employees) {
			if(employee.getInitials().equals(id)) {
				employees.remove(employee);
				return;
			}
		}
	}
}
