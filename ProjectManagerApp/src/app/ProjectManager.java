package app;

import java.util.ArrayList;
//Bastian
public class ProjectManager extends Employee {
	public ArrayList<Project> managerOfProjects = new ArrayList<Project>(); 
	
	public ProjectManager(String initials) {
		super(initials);
	}
	
	public ArrayList<Project> getProjects() {
		return managerOfProjects;
	}
	
	public boolean hasProject(int ID) {
		for(Project project : managerOfProjects) {
			if(project.getID() == ID) {
				return true;
			}
		}
		return false;
	}
	
	public void setDeadline(Project project, int endWeek, int endYear) throws OperationNotAllowedException {
		if(!hasProject(project.getID())) {
			throw new OperationNotAllowedException("Invalid project manager");
		} else {
			project.setDeadline(endWeek, endYear, this);
		}
	}
}
