##Victor
Feature: Remove project
	Description: 
	Actors: 

Scenario: The project manager removes the project
 	Given a project is already added to system 
 	And a project manager in the system with initials "xxxx" is added to the project
 	When the project manager removes the project
 	Then the project is in not the system
 	
Scenario: The project manager removes a non exixtent project
 	Given there isn't a project with ID 10
 	When the project manager removes the project with ID 10
 	Then the error message "Project does not exist in the system" is given
 	
 Scenario: User who is not project manager removes the project
 	Given a project is already added to system
 	And a projectmanager in the project with initials "yyyy"
 	And a user who is not the projects manager with initials "xxxx"
 	When the user removes the project
 	Then the error message "Only the project manager can remove the project" is given