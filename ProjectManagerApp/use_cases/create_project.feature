##Cecilie
Feature: Create a project 
	Description:  
	Actors: User

Scenario: The user creates a new project 
 	Given that there is a project with start week 10, end week 15 and year 2020
 	When the project is added to the system
 	Then the project is in the system 	

Scenario: The user creates a new project with negativ parameters
 	Given that there is a project with start week -10, end week -15 and year -2020
 	When the project is added to the system
 	Then the error message "Given invalid parameters" is given	
