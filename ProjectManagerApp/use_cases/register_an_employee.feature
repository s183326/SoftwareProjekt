##Theodor
Feature: register an employee
	Description: 
	Actors: 

Scenario: The user registers an employee by initials
 	Given there is an employee with initials "xxxx"
	When the user registers the employee
	Then the system contains the employee with initials "xxxx"
 	
Scenario: The user registers an already existing employee
 	Given an employee is already registered
 	When the user registers the employee
 	Then the error message "Employee is already registered" is given
 	
Scenario: The user registers an employee with invalid initials
	Given there is an employee with initials "xxxxx"
	When the user registers the employee
	Then the error message "Invalid employee initials" is given