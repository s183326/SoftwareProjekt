##Bastian
Feature: Assign projectmanager to a project 
	Description:  
	Actors: User

Scenario: The user assigns a project manager to a project
 	Given a project is already added to system
 	And a project manager in the system with initials "xxxx"
 	When the project manager is added to the project
 	Then the project has the project manager

Scenario: The user assigns the same project manager to the project
 	Given a project is already added to system
 	And a project manager in the system with initials "xxxx" is added to the project
 	When the same project manager is added to the project
 	Then the error message "Cannot add the same projectmanager" is given

Scenario: The user assigns an employee to project manager
 	Given a project is already added to system
 	And an employee with initials "T" is added to the system
 	When the employee is assigned to project mananger for the project
 	Then the error message "Employee cannot be a project manager" is given


