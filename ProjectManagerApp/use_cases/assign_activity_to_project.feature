##Alexander
Feature: Assign activity to a project 
	Description:  
	Actors: User

Scenario: The user assigns an activity to a project
 	Given a project is already added to system
 	And a projectmanager in the project with initials "xxxx"
 	When the project manager assigns an activity "Do something" with expected 12 to the project
 	Then the project has the activity

 Scenario: The user who is not the project manager assigns an activity to a project
 	Given a project is already added to system
 	And a projectmanager in the project with initials "yyyy"
 	And a user who is not the projects manager with initials "xxxx"
 	When the user assigns an activity "Do something" with expected 12 to the project
 	Then the error message "Only the project manager can assign an activity" is given