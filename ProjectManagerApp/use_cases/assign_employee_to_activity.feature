##Alexander
Feature: Assign employee to activity
	Description:  
	Actors: User

Scenario: The user assigns an avaliable employee to an activity for a project
	Given a project is already added to system
	And a project manager in the system with initials "xxxx" is added to the project
	And an employee is already registered
	And the employee is added to the project
  And an activity with the name "activity" and expected time of 8 hours is added to the project
  When the project manager assigns the employee to the activity
  Then the employee is assigned to the activity
	
Scenario: The user tries to assign an unavailable employee to an activity for a project
	Given a project is already added to system
	And a project manager in the system with initials "xxxx" is added to the project
	And an employee is already registered
	And the employee is added to the project
	And the employee is assigned to 20 activities
  And an activity with the name "activity" and expected time of 8 hours is added to the project
  When the project manager assigns the employee to the activity
  Then the error message "Employee has too many activities" is given

Scenario: The user assigns an employee a private activity
	Given an employee is already registered
	When an activity with the name "holiday" and expected time of 30 hours is added to the employee
	Then the employee is assigned to the private activity
	