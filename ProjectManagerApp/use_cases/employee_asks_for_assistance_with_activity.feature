##Cecilie
Feature: Employee asks for assistance with activity
	Description:  
	Actors: User

Scenario: The employee can request for help with a specific activity
 	Given a project is already added to system 
 	And a project manager in the system with initials "xxxx" is added to the project
 	And an employee is already registered
 	And the employee is added to the project
 	And an activity with the name "activity" and expected time of 8 hours is added to the project
  And the project manager assigns the employee to the activity
 	Then that employee can assign another employee to the same activity