##Bastian
Feature: Assign employee to project
	Description:  
	Actors: Project manager

Scenario: Project manager assigns employee to project
	Given a project is already added to system
 	And a project manager in the system with initials "xxxx" is added to the project
 	And an employee with initials "TG" is in the system 
 	When the project manager assigns the employee to the project
 	Then the employee is assigned to the project

Scenario: Employee assigns employee to project
	Given a project is already added to system
 	And a employee in the system with initials "xxxx" is added to the project
 	And an employee with initials "TG" is in the system 
 	When the employee assigns the employee to the project
 	Then the error message "Only the project manager can assign an employee to a project" is given
 
