##Victor
Feature: Set deadline for project
    Description: The project leader sets project deadline. 
    Actors: Project manager

Scenario: Project manager sets project deadline succesfully
   	Given a project is already added to system
  	And a project manager in the system with initials "xxxx" is added to the project
  	When the project manager sets new deadline for the project with week 12 and year 2020. 
  	Then the project deadline is set
  	
Scenario: Another project manager sets project deadline when not project manager for that project
   	Given a project is already added to system
  	And a project manager in the system with initials "xxxx" is added to the project
  	When a project manager with initials "yyyy" sets new deadline for the project with week 12 and year 2020
  	Then the error message "Invalid project manager" is given

Scenario: Employee sets deadline for project
   	Given a project is already added to system
  	And a project manager in the system with initials "xxxx" is added to the project
  	When an employee with initials "yyyy" sets new deadline for the project with week 12 and year 2020
  	Then the error message "Only project managers are allowed to change the deadline" is given

Scenario: Project manager sets project deadline before the start
    Given a project is already added to system 
  	And a project manager in the system with initials "xxxx" is added to the project
  	When the project manager sets new deadline for the project with week 9 and year 2020. 
  	Then the error message "Invalid deadline" is given