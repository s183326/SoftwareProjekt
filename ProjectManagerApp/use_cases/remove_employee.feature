##Victor
Feature: Remove employee
	Description: 
	Actors: 

Scenario: The user removes an employee by initials
 	Given an employee is already registered
	When the user removes the employee
	Then the system does not contain the employee
 	
Scenario: The user removes an non existent employee
 	Given there isn't an employee with initials "abcd"
 	When the user removes the employee "abcd"
 	Then the error message "Employee does not exist in the system" is given