##Theodor
Feature: Employee registers time spend
	Description:  
	Actors: User

Scenario: Employee adds time spend on activity
 	Given a project is already added to system
 	And a project manager in the system with initials "xxxx" is added to the project
 	And an employee is already registered
 	And the employee is added to the project
	And an activity with the name "activity" and expected time of 8 hours is added to the project
	When the project manager assigns the employee to the activity
	When the employee registers 4 hours spend on the activity
	Then 4 hours is registered to the activity

Scenario: Employee adds time spend on activity
 	Given a project is already added to system
 	And a project manager in the system with initials "xxxx" is added to the project
 	And an employee is already registered
 	And the employee is added to the project
	And an activity with the name "activity" and expected time of 8 hours is added to the project
	When the project manager assigns the employee to the activity
	When the employee registers -4 hours spend on the activity
	Then the error message "Can not register negativ time" is given
	

