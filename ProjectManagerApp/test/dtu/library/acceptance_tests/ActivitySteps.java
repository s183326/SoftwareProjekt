package dtu.library.acceptance_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.Activity;
import app.Application;
import app.Employee;
import app.OperationNotAllowedException;
import app.Project;
import app.ProjectManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//Theodor
public class ActivitySteps {
	
	Project project;
	Activity activity;
	ProjectManager projectmanager;
	Application application;
	ErrorMessageHolder errormessageholder;
	Employee employee;

	public ActivitySteps(Application application, ErrorMessageHolder errormessageholder) {
		this.application = application;
		this.errormessageholder = errormessageholder;
	}
	

	@Given("a projectmanager in the project with initials {string}")
	public void aProjectmanagerInTheProjectWithInitials(String initials) throws OperationNotAllowedException {
		projectmanager = new ProjectManager(initials);
	    application.addEmployee(initials, true);
	    project = application.getProject(application.projectCounter);
	    application.addProjectmanagerToProject(projectmanager.getInitials(), project.getID());
	}
	//Cecilie
	@When("the project manager assigns an activity {string} with expected {int} to the project")
	public void theProjectManagerAssignsAnActivityWithExpectedToTheProject(String name, Integer expectedTime) throws OperationNotAllowedException {
		activity = new Activity(name, expectedTime);
		application.assignActivityToProject(name, expectedTime, project.getID(), projectmanager.getInitials());
	}
	
	@Then("the project has the activity")
	public void theProjectHasTheActivity() {
	    assertTrue(project.hasActivity(activity));
	}
	
	@Given("a user who is not the projects manager with initials {string}")
	public void aUserWhoIsNotTheProjectsManagerWithInitials(String initials) throws OperationNotAllowedException {
		employee = new Employee(initials);
		application.addEmployee(initials, false);
	    project = application.getProject(application.projectCounter);
		assertFalse(employee.isProjectManager(project));
	}

	@When("the user assigns an activity {string} with expected {int} to the project")
	public void theUserAssignsAnActivityWithExpectedToTheProject(String name, Integer expectedTime) throws OperationNotAllowedException {
		try {
			activity = new Activity(name, expectedTime);
			application.assignActivityToProject(name, expectedTime, project.getID(), employee.getInitials());
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
}
