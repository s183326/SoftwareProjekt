package dtu.library.acceptance_tests;
//Alexander
public class ErrorMessageHolder {
	private String errorMessage = "";

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
