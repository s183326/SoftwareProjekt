package dtu.library.acceptance_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.Application;
import app.OperationNotAllowedException;
import app.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//Victor
public class ProjectSteps {
	Project project;
	private Application application;
	ErrorMessageHolder errormessageholder;
	
	public ProjectSteps(Application application, ErrorMessageHolder errormessageholder) {
		this.application = application;
		this.errormessageholder = errormessageholder;
	}
	
	
	public ProjectSteps(Application application) {
		this.application = application;
	}
	
	@Given("that there is a project with start week {int}, end week {int} and year {int}")
	public void thatThereIsAProjectWithStartWeekEndWeekAndYear(int startWeek, int endWeek, int year) {
	    project = new Project(year, startWeek, endWeek);
	}

	@When("the project is added to the system")
	public void theProjectIsAddedToTheSystem() {
		
		try {
			application.addProject(project.getYear(), project.getStartWeek(), project.getDeadline());
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
	//Alexander
	@Then("the project is in the system")
	public void theProjectIsInTheSystem() throws OperationNotAllowedException {
		assertTrue(application.getProject(project.getID()+1) != null);
	}
	
	@Given("a project is already added to system")
	public void aProjectIsAlreadyAddedToSystem() throws OperationNotAllowedException {
		thatThereIsAProjectWithStartWeekEndWeekAndYear(10, 15, 2020);
		theProjectIsAddedToTheSystem();
	}
	
	@When("the project manager removes the project")
	public void theProjectManagerRemovesTheProject() throws OperationNotAllowedException {
	    application.projectManagerRemoveProject(1, "xxxx");
	}

	@Then("the project is in not the system")
	public void theProjectIsInNotTheSystem() {
	    assertFalse(application.checkProject(project.getID()));
	}
	//Bastian
	@Given("there isn't a project with ID {int}")
	public void thereIsnTAProjectWithID(Integer ID) {
		 assertFalse(application.checkProject(ID));
	}

	@When("the project manager removes the project with ID {int}")
	public void theUserRemovesTheProjectWithID(Integer ID) throws OperationNotAllowedException {
		try {
			application.projectManagerRemoveProject(ID, "xxxx");
		}catch(OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
	
	@When("the user removes the project")
	public void theUserRemovesTheProject() {
		try {
			application.projectManagerRemoveProject(1,"xxxx");
		}catch(OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
}
