package dtu.library.acceptance_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import app.Activity;
import app.Application;
import app.Employee;
import app.OperationNotAllowedException;
import app.Project;
import app.ProjectManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//Alexander
public class ProjectmanagerSteps {
	private ProjectManager projectmanager;
	private Application application;
	private ErrorMessageHolder errormessageholder;
	private Project project;
	private Activity activity;
	private Employee employee;
	private Employee employee2;
	private int endWeek;
	private int endYear;

	public ProjectmanagerSteps(Application application, ErrorMessageHolder errormessageholder) {
		this.application = application;
		this.errormessageholder = errormessageholder;
	}

	@Given("a project manager in the system with initials {string}")
	public void aProjectmanagerInTheSystemWithInitials(String initials) throws OperationNotAllowedException {
		projectmanager = new ProjectManager(initials);
		application.addEmployee(initials, false);
		application.assignEmployeeToProjectManager(initials);
	}

	@When("the project manager is added to the project")
	public void theProjectmanagerIsAddedToTheProject() throws OperationNotAllowedException {
		application.addProjectmanagerToProject(projectmanager.getInitials(), application.projectCounter);
		project = application.getProject(application.projectCounter);
		projectmanager.managerOfProjects.add(project);
	}

	@Then("the project has the project manager")
	public void theProjectHasTheProjectmanager() {
		ProjectManager pm = null;
		try {
			pm = project.getProjectManager();
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
		assertEquals(projectmanager.getInitials(), pm.getInitials());
	}

	@Given("a project manager in the system with initials {string} is added to the project")
	public void aProjectmanagerInTheSystemWithInitialsIsAddedToTheProject(String initials) throws OperationNotAllowedException {
		aProjectmanagerInTheSystemWithInitials(initials);
		theProjectmanagerIsAddedToTheProject();
	}

	@When("the same project manager is added to the project")
	public void theSameProjectmanagerIsAddedToTheProject() {
		try {
			application.addProjectmanagerToProject(projectmanager.getInitials(), application.projectCounter);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
//Theodor
	@When("the project manager sets new deadline for the project with week {int} and year {int}.")
	public void theProjectManagerSetsNewDeadlineForTheProjectWithWeekAndYear(Integer endWeek, Integer endYear) throws OperationNotAllowedException {
		this.endWeek = endWeek;
		this.endYear = endYear;
		try {
			application.projectManagerSetsDeadlineForProject(projectmanager.getInitials(), project.getID(), endWeek, endYear);
			//projectmanager.setDeadline(project, endWeek, endYear);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}

	@Then("the project deadline is set")
	public void theProjectDeadlineIsSet() {
		assertTrue(project.getDeadline() == endWeek);
		assertTrue(project.getYear() == endYear);
	}
	
	@When("a project manager with initials {string} sets new deadline for the project with week {int} and year {int}")
	public void aProjectManagerWithInitialsSetsNewDeadlineForTheProjectWithWeekAndYear(String initials, Integer endWeek, Integer endYear) throws OperationNotAllowedException {
		aProjectmanagerInTheSystemWithInitials(initials);
		try {
			theProjectManagerSetsNewDeadlineForTheProjectWithWeekAndYear(endWeek, endYear);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
	
	@When("an employee with initials {string} sets new deadline for the project with week {int} and year {int}")
	public void anEmployeeWithInitialsSetsNewDeadlineForTheProjectWithWeekAndYear(String initials, Integer endWeek, Integer endYear) throws OperationNotAllowedException {
		project = application.getProject(application.projectCounter);
		Employee emp = new Employee(initials);
		application.addEmployee(initials, false);
		try {
			project.setDeadline(endWeek, endYear, emp);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
	//Cecilie
	@Given("the employee is added to the project")
	public void theEmployeeIsAddedToTheProject() throws OperationNotAllowedException {
		employee = application.getEmployee("1234");
	    project = application.getProject(application.projectCounter);
	    application.addEmployeeToProject(project.getID(), employee.getInitials(), projectmanager.getInitials());
	    //Only the project manager can assign an employee to a project
	}
	
	@Given("an activity with the name {string} and expected time of {int} hours is added to the project")
	public void anActivityWithTheNameAndExpectedTimeOfHoursIsAddedToTheProject(String name, Integer expectedTime) throws OperationNotAllowedException {
	    application.assignActivityToProject(name, expectedTime, project.getID(), projectmanager.getInitials());
	    activity = application.getProject(project.getID()).searchActivity(name);
	}
	
	@When("the project manager assigns the employee to the activity")
	public void theProjectManagerAssignsTheEmployeeToTheActivity() throws OperationNotAllowedException {
	    try {
	    	application.assignEmployeeToActivity(project.getID(), activity.getName(), employee.getInitials(), projectmanager.getInitials());
	    	//project.searchActivity(activity.getName(), activity.getExpectedTime()).assignEmployee(employee, (ProjectManager) application.getEmployee(projectmanager.getInitials()));
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}
	
	@Then("that employee can assign another employee to the same activity")
	public void thatEmployeeCanAssignAnotherEmployeeToTheSameActivity() throws OperationNotAllowedException {
		Employee e2 = new Employee("yyyy");
		application.addEmployee(e2.getInitials(), false);
		application.employeeAskForAssistance(employee.initials, e2.initials, activity.getName());
		assertTrue(activity.containsEmployee(e2));
	}
	
	@Given("the employee is assigned to {int} activities")
	public void theEmployeeIsAssignedToActivities(Integer noOfActivities) throws OperationNotAllowedException {
	    for(int i = 0; i < noOfActivities; i++) {
	    	Activity a = new Activity("name", i);
	    	a.assignProject(project);
		    application.assignActivityToProject(a.getName(), a.getExpectedTime(), project.getID(), projectmanager.getInitials());
	    	try {
	    		a.assignEmployee(employee, projectmanager);
			} catch (OperationNotAllowedException e) {
				errormessageholder.setErrorMessage(e.getMessage());
			}
	    }
	}
	
	@Then("the employee is assigned to the activity")
	public void theEmployeeIsAssignedToTheActivity() throws OperationNotAllowedException {
	    assertTrue(project.searchActivity(activity.getName()).containsEmployee(employee));
	}
	
	@Then("the employee is assigned to the private activity")
	public void theEmployeeIsAssignedToThePrivateActivity() {
	    assertTrue(employee.hasActivity(activity));
	}
	
	@When("an activity with the name {string} and expected time of {int} hours is added to the employee")
	public void anActivityWithTheNameAndExpectedTimeOfHoursIsAddedToTheEmployee(String name, Integer duration) {
		try {
			employee = application.getEmployee("1234");
			application.assignPrivateActivity(employee.getInitials(), name, duration);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
		//Activity med samme parametre men andet objekt
	    activity = new Activity(name, duration);
	}
	
	@When("the employee registers {int} hours spend on the activity")
	public void theEmployeeRegistersHoursSpendOnTheActivity(int time) {
	    try {
			application.registerTimeSpendOnActivity(employee.getInitials(), project.getID(), activity.getName(), time);
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}

	@Then("{int} hours is registered to the activity")
	public void hoursIsRegisteredToTheActivity(int time) {
	    assertEquals(activity.timeSpendForEmployee(employee) ,time);
	}
	
	@Given("an employee with initials {string} is in the system")
	public void anEmployeeWithInitialsIsInTheSystem(String initials) throws OperationNotAllowedException {
		employee = new Employee(initials);
		application.addEmployee(initials, false);
	}

	@When("the project manager assigns the employee to the project")
	public void theProjectManagerAssignsTheEmployeeToTheProject() {
		try {
			application.addEmployeeToProject(1, employee.getInitials(), projectmanager.getInitials());
		} catch (OperationNotAllowedException e) {
			
		}
	}
	//Theodor
	@Then("the employee is assigned to the project")
	public void theEmployeeIsAssignedToTheProject() throws OperationNotAllowedException {
		assertTrue(application.getProject(1).checkEmployee(employee.getInitials()));
	}
	
	@Given("a employee in the system with initials {string} is added to the project")
	public void aEmployeeInTheSystemWithInitialsIsAddedToTheProject(String initials) throws OperationNotAllowedException {
		employee2 = new Employee(initials);
		application.addEmployee(initials, false);
		
		aProjectmanagerInTheSystemWithInitials("G");
		theProjectmanagerIsAddedToTheProject();
		
		application.addEmployeeToProject(project.getID(), employee2.getInitials(), projectmanager.getInitials());
		
		
	}

	@When("the employee assigns the employee to the project")
	public void theEmployeeAssignsTheEmployeeToTheProject() {
		try {
			application.addEmployeeToProject(project.getID(), employee.getInitials(), employee2.getInitials());
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}

	@Given("an employee with initials {string} is added to the system")
	public void an_employee_with_initials_is_added_to_the_system(String initials) throws OperationNotAllowedException {
	    employee = new Employee(initials);
	    application.addEmployee(initials, false);
	}

	@When("the employee is assigned to project mananger for the project")
	public void the_employee_is_assigned_to_project_mananger_for_the_project() {
	    try {
			application.addProjectmanagerToProject(employee.initials, application.getProject(application.projectCounter).getID());
		} catch (OperationNotAllowedException e) {
			errormessageholder.setErrorMessage(e.getMessage());
		}
	}


}
