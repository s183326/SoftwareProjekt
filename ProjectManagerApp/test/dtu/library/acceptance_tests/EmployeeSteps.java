package dtu.library.acceptance_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import app.Activity;
import app.Application;
import app.Employee;
import app.OperationNotAllowedException;
import app.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//Victor
public class EmployeeSteps {
	private Employee e;
	private Application application;
	private ErrorMessageHolder errorMessageHolder;
	private Project project;
	private Activity activity;
	
	
	public EmployeeSteps(Application application, ErrorMessageHolder errorMessageHolder) {
		this.application = application;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("there is an employee with initials {string}")
	public void thereIsAnEmployeeWithInitials(String initials) {
	    e = new Employee(initials);
	}
 //Bastian
	@When("the user registers the employee")
	public void theUserRegistersTheEmployee() throws OperationNotAllowedException {
		try {
			application.addEmployee(e.getInitials(), false);
		} catch (OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}

	@Then("the system contains the employee with initials {string}")
	public void the_system_contains_the_employee_with_initials(String initials) {
		assertTrue(application.checkEmployee(initials));
	}
	
	@Given("an employee is already registered")
	public void anEmployeeIsAlreadyRegistered() throws OperationNotAllowedException {
	    e = new Employee("1234");
	    application.addEmployee(e.getInitials(), false);
	}

	@Then("the error message {string} is given")
	public void theErrorMessageIsGiven(String errorMessage) {
		assertEquals(errorMessage, this.errorMessageHolder.getErrorMessage());
	}
	
	@When("the user removes the employee")
	public void theUserRemovesTheEmployee() throws OperationNotAllowedException {
		application.removeEmployee(e.getInitials());
	}
	//Victor
	@Then("the system does not contain the employee")
	public void theSystemDoesNotContainTheEmployee() {
		assertFalse(application.checkEmployee("1234"));
	}
	
	@Given("there isn't an employee with initials {string}")
	public void thereIsnTAnEmployeeWithInitials(String initials) {
	    assertFalse(application.checkEmployee(initials));
	}

	@When("the user removes the employee {string}")
	public void theUserRemovesTheEmployee(String initials) throws OperationNotAllowedException {
		try {
			application.removeEmployee(initials);
		}catch(OperationNotAllowedException e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
		
	}
	
	
}
