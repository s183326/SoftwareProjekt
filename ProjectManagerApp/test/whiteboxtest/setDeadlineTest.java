package whiteboxtest;

import static org.junit.Assert.*;
import org.junit.Test;
import app.Project;
import app.ProjectManager;
import app.Employee;
import app.OperationNotAllowedException;
//Cecilie

public class setDeadlineTest {

	
	Project project = new Project(2020, 10, 15);
	
	@Test
	public void testA() {
		String errorM = "";
		Employee employee = new Employee("JIM");
		try {
			project.setDeadline(10, 15, employee);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Only project managers are allowed to change the deadline");
	}
	
	@Test
	public void testB() {
		String errorM = "";
		int endWeek = 9;
		int endYear = 2019;
		ProjectManager projectmanager = new ProjectManager("BOB");
		try {
			project.setDeadline(endWeek, endYear, projectmanager);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Invalid deadline");
	}
	@Test
	public void testC() throws OperationNotAllowedException {
	
		int endWeek = 15 ;
		int endYear = 2020;

		assertEquals(endWeek, project.getDeadline());
		assertEquals(endYear, project.getYear());

	}
	
}

