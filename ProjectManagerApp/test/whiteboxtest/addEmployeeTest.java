package whiteboxtest;

import static org.junit.Assert.*;

import org.junit.Test;

import app.Application;
import app.OperationNotAllowedException;
//Theodor
public class addEmployeeTest {

	Application app = new Application();
	
	@Test
	public void testA() {
		String initials = "";
		String errorM = "";
		try {
			app.addEmployee(initials, false);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Invalid employee initials");
	}
	
	@Test
	public void testB() {
		String initials = "ABCDE";
		String errorM = "";
		try {
			app.addEmployee(initials, false);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Invalid employee initials");
	}
	
	@Test
	public void testC() throws OperationNotAllowedException {
		String initials = "TG";
		app.addEmployee(initials, false);
		
		String errorM = "";
		try {
			app.addEmployee(initials, false);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Employee is already registered");
	}
	
	@Test
	public void testD() throws OperationNotAllowedException {
		String initials = "TG";
		boolean pm = false;
		app.addEmployee(initials, pm);
		
		assertEquals(initials, app.getEmployee(initials).getInitials());
		
	}
	
	@Test
	public void testE() throws OperationNotAllowedException {
		String initials = "TG";
		boolean pm = true;
		app.addEmployee(initials, pm);
		
		assertEquals(initials, app.getEmployee(initials).getInitials());
		
	}

}
