package whiteboxtest;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import app.Application;
import app.OperationNotAllowedException;
//Bastian
public class addProjectTest {
	Application app = new Application();
	int year;
	int startWeek;
	int endWeek;
	String errorM;
	
	@Test
	public void testA() {
		year = -1;
		startWeek = 20;
		endWeek = 25;
		errorM = "";
		
		try {
			app.addProject(year, startWeek, endWeek);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Given invalid parameters");
	}
	
	@Test
	public void testB() {
		year = 2020;
		startWeek = -1;
		endWeek = 25;
		errorM = "";
		
		try {
			app.addProject(year, startWeek, endWeek);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Given invalid parameters");
	}
	
	@Test
	public void testC() {
		year = 2020;
		startWeek = 20;
		endWeek = -1;
		errorM = "";
		
		try {
			app.addProject(year, startWeek, endWeek);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Given invalid parameters");
	}
	
	@Test
	public void testD() {
		year = 2020;
		startWeek = 20;
		endWeek = 25;
		errorM = "";
		
		try {
			app.addProject(year, startWeek, endWeek);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		
		try {
			assert app.getProject(app.projectCounter).getYear() == year && 
					app.getProject(app.projectCounter).getStartWeek() == startWeek &&
					app.getProject(app.projectCounter).getDeadline() == endWeek &&
					app.getProject(app.projectCounter).getID() == app.projectCounter;
		} catch (OperationNotAllowedException e) {
			//N�r aldirg herned
		}
	}
	
}
