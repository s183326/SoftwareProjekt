package whiteboxtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import app.Application;
import app.OperationNotAllowedException;
//Victor
public class assignEmployeeTest {
	
	Application app = new Application();

	@Test
	public void testA() throws OperationNotAllowedException {
		String errorM = "";
		String initialsE = "T";
		String initialsP = "V";
		String actName = "job";
		try {
			app.addEmployee(initialsP, true);
			app.addEmployee(initialsE, false);
			app.addProject(2020, 1, 2);
			app.addProjectmanagerToProject(initialsP, 1);
			app.assignActivityToProject(actName, 10, 1, initialsP);
			app.assignEmployeeToActivity(1, actName, initialsE, initialsP);
		}catch(OperationNotAllowedException e){
			errorM = e.getMessage();
		}
		assertEquals(initialsE, app.getProject(1).getActivities().get(0).getEmployees().get(0).getInitials());
	}

	@Test
	public void testB() throws OperationNotAllowedException {
		String errorM = "";
		String initialsE = "T";
		String initialsP = "V";
		String actName = "job";
		try {
			app.addEmployee(initialsP, true);
			app.addEmployee(initialsE, false);
			app.addEmployee("B", true);
			app.addProject(2020, 1, 2);
			app.addProjectmanagerToProject(initialsP, 1);
			app.assignActivityToProject(actName, 10, 1, initialsP);
			app.assignEmployeeToActivity(1, actName, initialsE, "B");
		}catch(OperationNotAllowedException e){
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Invalid project manager");
	}
	
	@Test
	public void testC() {
		String errorM = "";
		String initialsE = "T";
		String initialsP = "V";
		String actName = "job";
		try {
			app.addEmployee(initialsP, true);
			app.addEmployee(initialsE, false);
			app.addProject(2020, 1, 2);
			for(int i = 0; i < 20; i++) {
				app.assignPrivateActivity(initialsE, Integer.toString(i), 2);
			}
			app.addProjectmanagerToProject(initialsP, 1);
			app.assignActivityToProject(actName, 10, 1, initialsP);
			app.assignEmployeeToActivity(1, actName, initialsE, initialsP);
		}catch(OperationNotAllowedException e){
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Employee has too many activities");
	}

	@Test
	public void testD() {
		String errorM = "";
		String initialsE = "T";
		String initialsP = "V";
		String actName = "job";
		try {
			app.addEmployee(initialsP, true);
			app.addEmployee(initialsE, false);
			app.addEmployee("B", true);
			app.addProject(2020, 1, 2);
			for(int i = 0; i < 20; i++) {
				app.assignPrivateActivity(initialsE, Integer.toString(i), 2);
			}
			app.addProjectmanagerToProject(initialsP, 1);
			app.assignActivityToProject(actName, 10, 1, initialsP);
			app.assignEmployeeToActivity(1, actName, initialsE, "B");
		}catch(OperationNotAllowedException e){
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Invalid project manager");
	}
}

