package whiteboxtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import app.Application;
import app.Project;
import app.ProjectManager;
import app.OperationNotAllowedException;
//Alexander
public class searchActivityTest {

	Application app = new Application();

	@Test
	public void testA() {
		Project project = new Project(2000, 10, 20);
		String act = "ABC";
		String errorM = "";

		try {
			project.searchActivity(act);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Activity does not exist in project");
	}

	@Test
	public void testB() throws OperationNotAllowedException {
		ProjectManager employee;
		employee = new ProjectManager("Alex");
		Project project = new Project(2000, 10, 20);
		project.assignProjectmanager(employee);
		project.assignActivity("ABC", 10, employee);
		String act = "ABC";

		assertEquals("ABC", project.searchActivity(act).getName());
	}

	@Test
	public void testC() throws OperationNotAllowedException {
		ProjectManager employee;
		employee = new ProjectManager("Alex");
		Project project = new Project(2000, 10, 20);
		project.assignProjectmanager(employee);
		project.assignActivity("ABC", 10, employee);
		String act = "DEF";
		String errorM = "";

		try {
			project.searchActivity(act);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Activity does not exist in project");
	}

	@Test
	public void testD() throws OperationNotAllowedException {
		ProjectManager employee;
		employee = new ProjectManager("Alex");
		Project project = new Project(2000, 10, 20);
		project.assignProjectmanager(employee);
		project.assignActivity("ABC", 10, employee);
		project.assignActivity("DEF", 10, employee);
		String act = "GHI";
		String errorM = "";

		try {
			project.searchActivity(act);
		} catch (OperationNotAllowedException e) {
			errorM = e.getMessage();
		}
		assertEquals(errorM, "Activity does not exist in project");
	}

	@Test
	public void testE() throws OperationNotAllowedException {
		ProjectManager employee;
		employee = new ProjectManager("Alex");
		Project project = new Project(2000, 10, 20);
		project.assignProjectmanager(employee);
		project.assignActivity("ABC", 10, employee);
		project.assignActivity("DEF", 10, employee);
		String act = "DEF";
		
		assertEquals("DEF", project.searchActivity(act).getName());
	}

}
